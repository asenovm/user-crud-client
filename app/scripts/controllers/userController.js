'use strict';

angular.module('crudClient')
  .controller('UserController', ['$scope', 'userService', '$log', function ($scope, userService, $log) {

    function fetchUsers() {
        userService.fetchUsers().then(function (data) {
            var users = data.data;
            _.each(users, function (user) {
                var time = new Date(user.birthDate);
                user.birthDate = time.getDate() + '-' + (time.getMonth() + 1) + '-' + time.getFullYear();
                user.birthDateTime = time.getTime();
            });
            $scope.users = users;
        }, function (err) {
            $log.error(err);
        });
    }

    fetchUsers();

    $scope.createUser = function (user) {
        userService.createUser(user).then(function () {
            fetchUsers();
            $scope.$broadcast('userCreated');
        }, function (err) {
            $log.error(err);
        });
    };

    $scope.deleteUser = function (user) {
        userService.deleteUser(user).then(function () {
            fetchUsers();
        }, function (err) {
            $log.error(err);
        });
    };

  }]);
