'use strict';

angular.module('crudClient')
    .service('userService', ['$http', function ($http) {

    var SERVER_URL = 'http://localhost:8080',
        PATH_USERS = '/users';

    this.fetchUsers = function fetchUsers () {
        return $http.get(SERVER_URL + PATH_USERS);
    };

    this.createUser = function createUser (user) {
        return $http.post(SERVER_URL + PATH_USERS, {
            firstName: user.firstName,
            lastName: user.lastName,
            email: user.email,
            birthDate: user.birthDate
        });
    };

    this.deleteUser = function deleteUser(user) {
        return $http.delete(SERVER_URL + PATH_USERS + '/' + user.email);
    };

}]);
