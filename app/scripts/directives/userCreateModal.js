'use strict';

angular.module('crudClient')
    .directive('userCreateModal', [function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'views/userCreateModal.html',
        link: function ($scope, element, attrs) {
            $scope.$on('userCreated', function () {
                $('#userCreateModal').modal('hide');
            });
        }
    };
}]);
