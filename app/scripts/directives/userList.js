'use strict';

angular.module('crudClient')
    .directive('userList', [function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'views/userList.html'
    };
}]);
